<?php
class MyClass
{
    private $imAPrivateProperty = 23;
    public $imAPublicProperty = 233;
    protected $imAProtectedProperty = 423;
}

$objA_MyClass = new MyClass();
$objB_MyClass = new MyClass();
$objC_MyClass = new MyClass();


echo $objA_MyClass->imAPublicProperty . "<br>";
$objA_MyClass->imAPublicProperty = "Hello";
echo $objA_MyClass->imAPublicProperty . "<br>";
?>

<?php
class MyClass1
{
    private $imAPrivateProperty = 33;
    public $imAPublicProperty = 343;
    protected $imAProtectedProperty = 434;

    public function doSomeThingPublic()
    {
        $this->doSomeThingProtected();
        echo "I'm inside on =>> " . __METHOD__ . "<br/>";
    }


    private function doSomeThingPrivate()
    {
        echo "I'm inside on =>> " . __METHOD__ . "<br/>";
    }


    protected function doSomeThingProtected()
    {
        echo "Value of imAPrivateProperty = " . $this->imAPrivateProperty . "<br/>";
        $this->doSomeThingPrivate();
        echo "I'm inside on =>> " . __METHOD__ . "<br/>";
    }
} //end of myclass

class MyChildClass extends MyClass1
{
    public function childMethodPublic()
    {
        $this->doSomeThingProtected();
    }
}

$obj_MyClass1 = new MyClass1;
echo $obj_MyClass1->imAPublicProperty . "<br/>";
$obj_MyClass1->doSomeThingPublic();



?>
<?php
echo "<hr/>";
class MyParent
{
    private $myPrivateProperty = "Hello";

    public function setMyPrivateProperty($myPrivatePropertyValue)
    {
        $this->myPrivateProperty = $myPrivatePropertyValue;
    }

    public function getMyPrivateProperty()
    {
        return $this->myPrivateProperty;
    }
} //end of MyParent


$objMyParent = new MyParent();
$objMyParent->setMyPrivateProperty($objMyParent->getMyPrivateProperty() . " World ");


?>