<?php

namespace App;

/**
 * namespaces defined as using encapsulate data/items;
 * If the class, function, variable/constants etc named are same and there might be problem in program;
 * so to avoid the confliction those of problems we are using Namespaces method;
 * see the PHP manual 
 */

class MyClass
{
    public $myPublicProperty = "Public Property of App\MyClass";
    public function doSomeThing()
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
    }
} //end of the MyClass
?>

<?php

namespace Bitm;

use App\MyClass as AppClass;

class MyClass
{
    public $myPublicProperty = "Public Property of Bitm\MyClass";
    public function doSomeThing()
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
    }
} //end of the MyClass
?>

<?php
$objAppMyClass = new \App\MyClass();
$objAppMyClass1 = new \Bitm\MyClass();
echo "MyPublicProperty = " . $objAppMyClass->myPublicProperty . "<br/>";
echo "MyPublicProperty = " . $objAppMyClass1->myPublicProperty . "<br/>";


?>
<?php

namespace Bitm;

class MyChildClass
{
    public $myPublicProperty = "Public Property of Bitm\MyChildClass";
    public function doSomeThing()
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
    }
} //end of the MyChildClass

$objMyChildClass = new MyChildClass();
// $objAnother = new MyChildClass;

// var_dump($objMyChildClass);

echo $objMyChildClass->myPublicProperty;
?>