<?php

/**
 * constructor method
 * destructor method
 * __call() method
 */
class MyClass
{

    public $myProperty;

    public function __construct()
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
    }

    public function __destruct()
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
    }

    public function __call($name, $arguments)
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
        echo "name= $name" . "<br/>";
        echo "arguments : <br/>";
        print_r($arguments);
    }
    public function __callStatic($name, $arguments)

    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
        echo "name= $name" . "<br/>";
        echo "arguments : <br/>";
        print_r($arguments);
    }

    public function __set($name, $value)
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
        echo "wrong property name = $name" . "<br/>";
        echo "wrong property value = $value" . "<br/>";
    }

    public function __unset($name)
    {
        echo "Wrong Property name= $name " . "<br/>";
    }

    public function __get($name)
    {
        echo "wrong property name = $name" . "<br/>";
    }

    public function __isset($name)
    {
        echo "wrong Property name = $name" . "<br/>";
    }

    public function __toString()
    {
        return "You can't echo an OBJECT" . "<br/>";
    }

    public function myMethod()
    {
        echo "I'm inside on " . __METHOD__ . "<br/>";
    }
} //end of MyClass

$obj = new MyClass();
$obj->myMethod();

unset($obj);

$anotherObj = new MyClass();

$anotherObj->doSomeThing("Hello World!", "World is not enough friendly for mankind");

MyClass::processAStaticFunction("Hello static World!", "World is not enough friendly for mankind");

$anotherObj->unknownProperty = "Unstructured Value for this property";

unset($anotherObj->unknownPropertyUnset);
echo $anotherObj->aPropertyThatDoesNotExit;

if (isset($anotherObj->aPropertyThatDoesNotExit)) {
    echo "Found" . "<br/>";
} else {
    echo "Not Found" . "<br/>";
}

echo $anotherObj;
