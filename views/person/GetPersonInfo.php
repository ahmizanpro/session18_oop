<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js">
    </script>
    <title>Form personal</title>
</head>

<body>
    <div class="container">
        <div>
            <h1 style="text-align:center">Person Information Form</h1>
            <form action="store.php" method="post">
                <div class="form-group">
                    <label for="Name">Name</label>
                    <input type="text" class="form-control" name='Name' id="Name" placeholder="Enter your name ">
                </div>
                <div class="form-group">
                    <label for="DOB">Date of Birth</label>
                    <input type="datetime-local" name='DOB' class="form-control" id="DOB" placeholder="mm/dd/yy">
                </div>
                <div class="form-group">
                    <label for="Gender">Gender :</label> &nbsp;&nbsp;
                    <input type="radio" name="Gender" id="Gender" value="Male"> Male&nbsp;&nbsp;
                    <input type="radio" name="Gender" id="Gender" value="Female"> Female&nbsp;&nbsp;
                    <input type="radio" name="Gender" id="Gender" value="Other"> Other&nbsp;&nbsp;
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>

    <script>
        $(function() {
            $("#DOB").datepicker();
            defaultDate: "09/22/2019"
        });
    </script>

</body>

</html>