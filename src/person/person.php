<?php

namespace App;

// class person
// {
//     public $id;
//     public $name;
//     public $dob;
//     public $gender;


//     public function setData($personInfoArray)
//     {

//         if (array_key_exists('id', $personInfoArray)) {
//             $this->id = $personInfoArray['id'];
//         }

//         if (array_key_exists('Name', $personInfoArray)) {
//             $this->name = $personInfoArray['Name'];
//         }
//         if (array_key_exists('DOB', $personInfoArray)) {
//             $this->dob = $personInfoArray['DOB'];
//         }
//         if (array_key_exists('Gender', $personInfoArray)) {
//             $this->gender = $personInfoArray['Gender'];
//         }
//     } //end of the SetData Method


//     public function view()
//     {
//         $name = $this->name;
//         $dob = $this->dob;
//         $gender =  $this->gender;

//         $varArray = array('name', 'dob', 'gender');
//         $singlePersonInfo = compact($varArray);
//         return $singlePersonInfo;
//     }
// }

class Person
{

    public $name;
    public $dob;
    public $gender;

    public function setData($postArray)
    {

        if (array_key_exists("Name", $postArray)) {
            $this->name = $postArray['Name'];
        }
        if (array_key_exists("DOB", $postArray)) {
            $this->dob = $postArray['DOB'];
        }
        if (array_key_exists("Gender", $postArray)) {
            $this->gender = $postArray['Gender'];
        }
    } //end of the setData Method


    public function view()
    {
        // if (!isset($_SESSION)) session_start();


        $name = $this->name;
        $dob = $this->dob;
        $gender = $this->gender;

        $varArray = array('name', 'dob', 'gender');
        $singlePersonInfo = compact($varArray);

        return $singlePersonInfo;
    }
}//end of Person Class